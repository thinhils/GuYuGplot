﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;

namespace GuYuGplot.Drawing.Elements
{
    class CircleNode:AbsNodeBase,IDrawing
    {
        #region IDrawing 成员

        public void Draw(System.Drawing.Graphics graphics)
        {
            RectangleF rect = new RectangleF(this.Location.X,this.Location.Y,this.Size.Width,this.Size.Height);
            LinearGradientBrush brush = new LinearGradientBrush(rect,this.Color,Color.Black,45);
            graphics.FillEllipse(brush, rect);

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;
            graphics.DrawString(this.Text, new Font("宋体", this.TextSize), new SolidBrush(this.TextColor), new RectangleF(this.Location.X, this.Location.Y, this.Size.Width, this.Size.Height), format);
        }

        #endregion
    }
}
